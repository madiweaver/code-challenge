import faker from 'faker';
import { Species, Rhinoceros } from '../src/rhinoceros';

export default function mockRhino(): Rhinoceros {
  return {
    id: faker.random.uuid(),
    name: faker.random.word(),
    species: faker.random.arrayElement(Species),
  };
}
