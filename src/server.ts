import Koa from 'koa';
import bodyParser from 'koa-bodyparser';
import router from './router';
import RhinocerosModel from './rhinoceros';

export default function createServer(model: RhinocerosModel): Koa {
  const app = new Koa();

  app.proxy = true;

  app.use(bodyParser());

  app.use(async (ctx, next) => {
    // eslint-disable-next-line no-console
    console.log('request received', { method: ctx.method, path: ctx.path });
    await next();
  });

  app.use(router(model).routes());

  return app;
}
