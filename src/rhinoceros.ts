import uuidv4 from 'uuid/v4';
import AJV from 'ajv';
import intersection from 'lodash/intersection';
import rhinoceroses from './data';
import { ValidationError } from './errors';

const ajv = new AJV();

export const Species = [
  'white_rhinoceros',
  'black_rhinoceros',
  'indian_rhinoceros',
  'javan_rhinoceros',
  'sumatran_rhinoceros',
];

export interface Rhinoceros {
  id: string;
  name: string;
  species: string;
}

export interface RhinocerosFilter {
  name?: string;
  species?: string;
}

export interface ProtoRhinoceros {
  name: string;
  species: string;
}

const validators = {
  create: ajv.compile({
    type: 'object',
    properties: {
      name: {
        type: 'string',
        minLength: 1,
        maxLength: 20,
      },
      species: {
        type: 'string',
        enum: Species,
      },
    },
    required: ['name', 'species'],
    additionalProperties: false,
  }),
};

export default class RhinocerosModel {
  private store: Rhinoceros[];

  private idIndex = new Map<string, Rhinoceros>();

  private nameIndex = new Map<string, Rhinoceros[]>();

  private speciesIndex = new Map<string, Rhinoceros[]>();

  constructor(store: Rhinoceros[] = rhinoceroses) {
    this.store = store;

    this.store.forEach((rhino) => this.addToIndexes(rhino));
  }

  public addToIndexes(rhino: Rhinoceros): void {
    this.idIndex.set(rhino.id, rhino);

    if (!this.nameIndex.has(rhino.name)) {
      this.nameIndex.set(rhino.name, []);
    }
    this.nameIndex.get(rhino.name).push(rhino);

    if (!this.speciesIndex.has(rhino.species)) {
      this.speciesIndex.set(rhino.species, []);
    }
    this.speciesIndex.get(rhino.species).push(rhino);
  }

  public getAll(query: RhinocerosFilter = {}): Rhinoceros[] {
    const sets = [];

    if (query.name) {
      sets.push(this.nameIndex.get(query.name));
    }

    if (query.species) {
      sets.push(this.speciesIndex.get(query.species));
    }

    if (sets.length < 1) {
      return this.store;
    }
    return intersection(...sets);
  }

  public getEndangered(): Rhinoceros[] {
    const endangeredRhinos: Rhinoceros[] = [];

    this.speciesIndex.forEach((rhinos) => {
      if (rhinos.length < 2) {
        endangeredRhinos.push(...rhinos);
      }
    });

    return endangeredRhinos;
  }


  public newRhinoceros(data: ProtoRhinoceros): Rhinoceros {
    const valid = validators.create(data);
    if (!valid) {
      throw new ValidationError(validators.create.errors);
    }

    const newRhino = {
      id: uuidv4(),
      name: data.name,
      species: data.species,
    };

    this.store.push(newRhino);
    this.addToIndexes(newRhino);

    return newRhino;
  }

  /**
   * finds a rhino by a given ID
   * if no rhino with the given ID is found, it returns undefined
   */
  public findById(id: string): Rhinoceros | undefined {
    return this.idIndex.get(id);
  }
}
